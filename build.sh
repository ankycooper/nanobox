builderimgname=bbmaker
finalimgname=nanobox
dir=.

#Clean up
docker builder prune -f
docker image rm "$builderimgname"  -f
docker image rm "$finalimgname" -f


#Complile busybox and RootFS
docker build -t "$builderimgname" -f "$dir/dockerfile.builder" "$dir"
docker run --rm "$builderimgname" tar cC rootfs . | xz -T0 -z9 > "$dir/busybox.tar.xz"

#build Final Image
docker build -t "$finalimgname" "$dir"

#Test
docker run --rm "$finalimgname" sh -xec 'true'
docker run -it --rm "$finalimgname" /bin/sh
rm busybox.tar.xz